const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      const cucumber = require('cypress-cucumber-preprocessor').default;
      const browserify = require('@cypress/browserify-preprocessor');
      const options = {
        ...browserify.defaultOptions,
        typescript: require.resolve('typescript'),
      };
      on('file:preprocessor', cucumber(options));
    },    
      "baseUrl": "http://www.google.com/",
      "specPattern": "**/*.feature",
      "excludeSpecPattern": ["integration/*.ts"]
  },
  // module: {
  //   rules: [
  //     {
  //       test: /\.js$/,
  //       exclude: /node_modules/,
  //       use: {
  //         loader: 'raw-loader',
  //       },
  //     },
  //   ],
  // },
  // entry: './path/to/my/entry/file.js',

});
