Feature: OpenMRS Login

  Scenario: User logs in with valid credentials
    Given I visit the OpenMRS login page
    When I enter valid credentials
    And I select Location
    And I click the login button
    Then I should be logged in successfully
