Feature: OrangeHRM Login

  Scenario: User logs in with valid credentials
    Given I visit the OrangeHRM login page
    When I enter valid credentials OrangeHRM
    And I click the login button OrangeHRM
    Then I should be logged in successfully in OrangeHRM
