/// <reference types="Cypress" />
import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps';

Given('I visit the OpenMRS login page', () => {
  cy.visit('https://demo.openmrs.org/openmrs/login.htm');
});

When('I enter valid credentials', () => {
  cy.get('#username').type('Admin');
  cy.get('#password').type('Admin123');
});

And('I select Location', () => {
  cy.get('li[id="Inpatient Ward"]').click();
});

And('I click the login button', () => {
  cy.get('input[id="loginButton"]').click();
});

Then('I should be logged in successfully', () => {
//   cy.url().should('include', '/coreapps/clinicianfacing/patient.page');
  cy.contains('Logged in as Super User').should('be.visible');
});
