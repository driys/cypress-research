/// <reference types="Cypress" />
import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps';

Given('I visit the OrangeHRM login page', () => {
  cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login');
});

When('I enter valid credentials OrangeHRM', () => {
  cy.get('input[name="username"]').type('Admin')
  cy.get('input[name="password"]').type('admin123')
});

And('I click the login button OrangeHRM', () => {
  cy.get('button[type="submit"]').click();
});

Then('I should be logged in successfully in OrangeHRM', () => {
  cy.url().should('include', '/dashboard');
  cy.contains('Dashboard').should('be.visible');
});
